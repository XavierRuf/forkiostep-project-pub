// 'use strict';
//
// const gulp = require('gulp');
// const sass = require('gulp-sass');
// const autoprefixer   = require('gulp-autoprefixer');
//
// sass.compiler = require('node-sass');
//
// gulp.task('sass', function () {
//   return gulp.src('src/scss/*.scss')
//     .pipe(sass().on('error', sass.logError))
//     .pipe(autoprefixer({
//       browsers: ['last 10 versions'],
//       cascade: false
//     }))
//     .pipe(gulp.dest('dist/css'));
// });
//
// gulp.task('watch', function() {
//   gulp.watch('src/scss/*.scss', gulp.series('sass'));
// });

'use strict';

let gulp = require('gulp');
let sass = require('gulp-sass');
let browserSync = require('browser-sync');
let autoprefixer   = require('gulp-autoprefixer');
// let imagemin   = require('gulp-imagemin');
// let cleanCSS = require('gulp-clean-css');

sass.compiler = require('node-sass');

gulp.task('scss', function () {
  return gulp.src('src/scss/*.scss')
  .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 10 versions'],
      cascade: false
    }))
  .pipe(gulp.dest('dist/css'))
  .pipe(browserSync.reload({stream: true}));
});


// gulp.task('imgs', function() {
//   return gulp.src("src/img/*.+(jpg|jpeg|png|gif)")
//     .pipe(imagemin({
//       progressive: true,
//       svgoPlugins: [{ removeViewBox: false }],
//       interlaced: true
//     }))
//     .pipe(gulp.dest("dist/img"))
// });

// gulp.task('minify-css', () => {
//   return gulp.src('dist/css/app.css')
//     .pipe(cleanCSS({compatibility: 'ie8'}))
//     .pipe(gulp.dest('dist/css'));
// });
//
gulp.task('browser-sync', function(done) {
  browserSync.init({
    server: {
      baseDir: "./"
    },
    //notify: false
    tunnel: true
  });
  done();
});


gulp.task('dev', gulp.parallel('browser-sync',  function () {
  gulp.watch('src/scss/*.scss', gulp.series('scss'))
}));
// gulp.task('dev', gulp.parallel('browser-sync', 'scss', 'minify-css'));



// gulp.task('watch', function() {
//   gulp.watch('src/scss/*.scss', gulp.series('sass'));
// });

